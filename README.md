# Bitbucket Pipelines Example

This repo uses Bitbucket Pipelines to build a Docker Image, creates a build tag commit, pushes the Docker Image to AWS Elastic Container Registry and deploys to AWS Elastic Beanstalk.

## What you need

* Access to an AWS account
* Bitbucket Pipelines Enabled

## Setup

* Create an AWS S3 bucket for your zip files to be stored in
* Create an AWS ECR for the image to be pushed to
* Create the Elastic Beanstalk app and environment for deployments
* Copy this repo to your bitbucket account
* Turn on Bitbucket Pipelines

## Configure Bitbucket Repo Variables

You will need to configure these variables that are found in the [bitbucket-pipelines.yml](bitbucket-pipelines.yml) file:

* APP_NAME - Name of your Elastic Beanstalk Application
* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY
* AWS_DEFAULT_REGION - Region you want to use
* APP_ENV_STAGING - Name of the staging app environment
* BUILD_BUCKET - S3 Bucket
* BUILD_PATH - S3 Sub-bucket
* DOCKER_IMAGE_URL - AWS ECR image url

## Credit

List coming soon
